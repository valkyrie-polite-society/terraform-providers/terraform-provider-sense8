package main

import (
    "github.com/hashicorp/terraform/helper/schema"
)

func resourceTransGirl() *schema.Resource {
    return &schema.Resource{
        Create: resourceTransGirlCreate,
        Read:   resourceTransGirlRead,
        Update: resourceTransGirlUpdate,
        Delete: resourceTransGirlDelete,

        Schema: map[string]*schema.Schema{
            "address": &schema.Schema{
                Type:     schema.TypeString,
                Required: true,
            },
        },
    }
}

func resourceTransGirlCreate(d *schema.ResourceData, m interface{}) error {
    return nil
}

func resourceTransGirlRead(d *schema.ResourceData, m interface{}) error {
    return nil
}

func resourceTransGirlUpdate(d *schema.ResourceData, m interface{}) error {
    return nil
}

func resourceTransGirlDelete(d *schema.ResourceData, m interface{}) error {
    return nil
}
